package ua.shpp.abstracts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.enums.LogLevel;

public abstract class AbstractLogger {
    Logger log = LoggerFactory.getLogger(AbstractLogger.class);
    LogLevel priority;

    public AbstractLogger(LogLevel priority) {
        this.priority = priority;
    }

    AbstractLogger next;

    public void setNext(AbstractLogger next) {
        this.next = next;
    }

    public void sendMessage(String message, LogLevel level) {
        if (priority.getValue() >= level.getValue()) {
            sendConcreteMessage(message, level);
        }
        if (next != null) {
            next.sendMessage(message, level);
        }
    }

    protected abstract void sendConcreteMessage(String message, LogLevel level);
}

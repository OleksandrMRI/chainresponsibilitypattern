package ua.shpp.enums;

import java.util.Arrays;

public enum LogLevel {
    INFO(3), WARN(2), ERROR(1);

    private final int level;

    LogLevel(int level) {
        this.level = level;
    }

    public int getValue() {
        return level;
    }
}

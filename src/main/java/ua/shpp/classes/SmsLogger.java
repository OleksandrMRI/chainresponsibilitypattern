package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.AbstractLogger;
import ua.shpp.enums.LogLevel;

public class SmsLogger extends AbstractLogger {
    Logger log = LoggerFactory.getLogger(SmsLogger.class);

    public SmsLogger(LogLevel priority) {
        super(priority);
    }

    protected void sendConcreteMessage(String message, LogLevel level) {
        log.error("{}. Sending message by SMS: {}", level, message);
    }
}

package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.AbstractLogger;
import ua.shpp.enums.LogLevel;

public class EMailLogger extends AbstractLogger {
    Logger log = LoggerFactory.getLogger(EMailLogger.class);

    public EMailLogger(LogLevel priority) {
        super(priority);
    }

    protected void sendConcreteMessage(String message, LogLevel level) {
        log.info("{}. Send message by e-mail: {}", level, message);
    }
}

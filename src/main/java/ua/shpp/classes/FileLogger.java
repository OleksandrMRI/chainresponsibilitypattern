package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.AbstractLogger;
import ua.shpp.enums.LogLevel;

public class FileLogger extends AbstractLogger {
    Logger log = LoggerFactory.getLogger(FileLogger.class);

    public FileLogger(LogLevel priority) {
        super(priority);
    }

    protected void sendConcreteMessage(String message, LogLevel level) {
        log.warn("{}. Write message to file: {}", level, message);
    }
}

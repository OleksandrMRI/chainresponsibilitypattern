package ua.shpp.chain_of_responsibility_pattern;

import ua.shpp.abstracts.AbstractLogger;
import ua.shpp.classes.EMailLogger;
import ua.shpp.classes.FileLogger;
import ua.shpp.classes.SmsLogger;
import ua.shpp.enums.LogLevel;

public class AppChainResponsibility {
    public static void main(String[] args) {
        AbstractLogger abstractLogger = new SmsLogger(LogLevel.ERROR);
        AbstractLogger abstractLogger1 = new FileLogger(LogLevel.WARN);
        AbstractLogger abstractLogger2 = new EMailLogger(LogLevel.INFO);
        abstractLogger.setNext(abstractLogger1);
        abstractLogger1.setNext(abstractLogger2);
        abstractLogger.sendMessage("Все добре", LogLevel.ERROR);
    }
}